//
//  AppDelegate.swift
//  KyotoCity
//
//  Created by 西川達哉 on 2016/11/07.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var detailTitle: String!
    var detailDescription: String!
    var detailUnivercities: String!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        initData()
        self.window!.tintColor = Colors.MainColor
        UINavigationBar.appearance().backgroundColor = UIColor.white
        
        let launchHelpers: [LaunchHelper] = [
            RealmLaunchHelper(),
            ResourceLaunchHelper()
            //            FabricLaunchHelper()
        ]
        launchHelpers.forEach { $0.setup(application: application) }

        return true
    }
    
    let dummy = DummyViewModel.sharedInstance
    
    func initData(){
        let model1 = SchoolData(name: "環太平洋大学", point: 170, address: "大阪府大阪市北区堂山町", eventPoint: 50, image: "noimage")
        
        
        let model2 = SchoolData(name: "神戸山手大学", point: 170, address: "兵庫県神戸市中央区", eventPoint: 50, image: "noimage")
        
        let model3 = SchoolData(name: "大阪青山大学", point: 170, address: "大阪府箕面市新稲", eventPoint: 50, image: "noimage")
        
        let model4 = SchoolData(name: "大阪保険医療大学", point: 170, address: "大阪府大阪市北区天満", eventPoint: 50, image: "noimage")
        
        dummy.model.value.append(model1)
        dummy.model.value.append(model2)
        dummy.model.value.append(model3)
        dummy.model.value.append(model4)
        
        
        
        
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

