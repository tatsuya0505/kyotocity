//
//  ListViewController.swift
//  KyotoCity
//
//  Created by takahashi tomoki on 2016/11/13.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ListViewController: UIViewController {
    let disposeBag = DisposeBag()

    @IBOutlet weak var logTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    static func instantiate() -> ListViewController {
        let storyBoard = UIStoryboard(name: "List", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: "list") as! ListViewController
    }
    
    let dummy = DummyViewModel()
    
    func initData(){
        let model1 = SchoolData(name: "環太平洋大学", point: 170, address: "大阪府大阪市北区堂山町", eventPoint: 50, image: "noimage")
        
        
        let model2 = SchoolData(name: "神戸山手大学", point: 170, address: "兵庫県神戸市中央区", eventPoint: 50, image: "noimage")
        
        let model3 = SchoolData(name: "大阪青山大学", point: 170, address: "大阪府箕面市新稲", eventPoint: 50, image: "noimage")
        
        let model4 = SchoolData(name: "大阪保険医療大学", point: 170, address: "大阪府大阪市北区天満", eventPoint: 50, image: "noimage")
        
        dummy.model.value.append(model1)
        dummy.model.value.append(model2)
        dummy.model.value.append(model3)
        dummy.model.value.append(model4)
        
        
        
        
    }

    func bind(){
        initData()
        
        let nib = UINib(nibName: "LogTableViewCell", bundle: nil)
        logTableView.register(nib, forCellReuseIdentifier: "Log")
        
        
        DummyViewModel.sharedInstance.model.asObservable()
            .bindTo(logTableView.rx.items(cellIdentifier: "Log", cellType: LogTableViewCell.self))
            { _, model, cell -> Void in
                cell.schoolName.text = model.name!
                cell.pointCount.text = "\(model.point!)"
                cell.addresLabel.text = "\(model.address!)"
                cell.eventPoint.text = "\(model.eventPoint!)"
                cell.spotName.image = UIImage(named: "\(model.image!)")
                
            }.addDisposableTo(disposeBag)

        
    }

}
