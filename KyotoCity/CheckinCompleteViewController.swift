import UIKit
import Bond
import RxSwift
import RxCocoa


class CheckinCompleteViewController: UIViewController {
    
    @IBOutlet weak var cancellButton: UIButton!
    

    
    override func loadView() {
        view = UIView.instantiateFromNib("CheckinCompleteView", owner: self)
        view.frame = UIScreen.main.bounds
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cancellButton.bnd_tap
            .observeNext {_ in
                self.dismiss(animated: true, completion: nil)
                let parentTabBarController = self.storyboard!.instantiateViewController(withIdentifier: "ParentTabBarController")
                self.present(parentTabBarController, animated: false, completion: nil)

        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
