import UIKit
import Bond
import SegueContext
import RxSwift

class MissionViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    
    private let missionViewModel = MissionViewModel.sharedInstance
    fileprivate let disposeBag = DisposeBag()

    
    override func loadView() {
        view = UIView.instantiateFromNib("MissionView", owner: self)
        view.frame = UIScreen.main.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.registerCell(type: MissionTableViewCell.self)
        missionViewModel.missions.asObservable()
            .bindTo(tableView.rx.items(cellIdentifier:"MissionTableViewCell", cellType:MissionTableViewCell.self)) { _, mission, cell -> Void in
                cell.mission = mission
            }.addDisposableTo(disposeBag)
        navigationItem.backBarButtonItem = backButtonItem
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.detailTitle = missionViewModel.missions.value[indexPath.row].title
        appDel.detailDescription = missionViewModel.missions.value[indexPath.row].description
        appDel.detailUnivercities = missionViewModel.missions.value[indexPath.row].univercityName
        self.performSegue(withIdentifier: "missiondetail")
        tableView.deselectRow(at:indexPath, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
