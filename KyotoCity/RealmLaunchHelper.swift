import UIKit
import RealmSwift

class RealmLaunchHelper: LaunchHelper {
    
    enum SchemaVersion {
        case initial
        
        static var latest: UInt64 {
            return SchemaVersion.initial.ver
        }
        
        var ver: UInt64 {
            return UInt64(self.hashValue)
        }
    }
    
    // MARK: - Publics
    internal func setup(application: UIApplication) {
        migrate()
        showPath()
        
    }
    
    // MARK: - Privates
    fileprivate func migrate() {
        let config = Realm.Configuration(
            schemaVersion: SchemaVersion.latest,
            migrationBlock: { migration, old in
                print(old)
                print(SchemaVersion.latest)
                if old < SchemaVersion.latest {
                    Realm.removeAllFiles()
                }
        })
        RealmFactory.sharedInstance.configuration = config
    }
    
    fileprivate func showPath() {
        if let path = RealmFactory.sharedInstance.realm().configuration.fileURL?.absoluteString {
            print("Realm path: \(path)")
        }
    }
    
}
