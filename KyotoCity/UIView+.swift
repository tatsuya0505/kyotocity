import UIKit

enum FadeType: TimeInterval {
    case normal = 0.2
    case slow = 1.0
}

extension UIView {
    
    func removeSubViews() {
        self.subviews.forEach { $0.removeFromSuperview() }
    }
    
    func fadeIn(_ type: FadeType = .normal, completed: (() -> ())? = nil) {
        fadeIn(type.rawValue, completed: completed)
    }
    
    func fadeIn(_ duration: TimeInterval = FadeType.slow.rawValue, completed: (() -> ())? = nil) {
        alpha = 0
        isHidden = false
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
        }, completion: { finished in
            completed?()
        })
    }
    
    func fadeOut(_ type: FadeType = .normal, completed: (() -> ())? = nil) {
        fadeOut(type.rawValue, completed: completed)
    }
    
    func fadeOut(_ duration: TimeInterval = FadeType.slow.rawValue, completed: (() -> ())? = nil) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0
        }, completion: { [weak self] finished in
            self?.isHidden = true
            self?.alpha = 1
            completed?()
        })
    }
    
    class func instantiateFromNib(_ nibName: String, owner: AnyObject) -> UIView {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: owner, options: nil).first as? UIView ?? UIView()
    }
    
}
