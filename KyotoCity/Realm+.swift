import RealmSwift

extension Realm {
    
    static func removeAllFiles() {
        guard let file = self.Configuration.defaultConfiguration.fileURL else { return }
        let files = [
            file,
            file.appendingPathExtension("lock"),
            file.appendingPathExtension("log_a"),
            file.appendingPathExtension("log_b"),
            file.appendingPathExtension("note")
        ]
        let manager = FileManager.default
        files.forEach {
            do {
                try manager.removeItem(at: $0)
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
}
