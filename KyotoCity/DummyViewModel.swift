//
//  DummyViewModel.swift
//  KyotoCity
//
//  Created by takahashi tomoki on 2016/11/13.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class DummyViewModel  {
    
    static let sharedInstance = DummyViewModel()
    
    
    
    var ishibashi = Variable<[String]>([])
    var point = Variable<[Int]>([])
    
    var model = Variable<[SchoolData]>([])
    
    
    var pointSum = Variable<Int>(0)
    
    
    
    
}
