import UIKit

class MissionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var univercitiesLabel: UILabel!
    @IBOutlet weak var PtLabel: UILabel!

    
    internal var mission: Mission? {
        didSet{
            guard let mission = self.mission else { return }
            self.iconImageView.image = UIImage(named: "icon_sample")
            self.titleLabel.text = mission.title
            self.univercitiesLabel.text = mission.univercityName
            self.PtLabel.text = "100Pt"
        }
    }
}
