//
//  LogTableViewCell.swift
//  KyotoCity
//
//  Created by takahashi tomoki on 2016/11/13.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit

class LogTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolName: UILabel!
    
    @IBOutlet weak var pointCount: UILabel!
    
    @IBOutlet weak var eventPoint: UILabel!
    
    @IBOutlet weak var spotName: UIImageView!
    
    @IBOutlet weak var addresLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
            
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
