import SwiftyJSON

struct Mission {
    
    internal let id: Int
    internal let title: String
    internal let description: String
    internal let difficulity: Int
    internal let requiredCheckin: [Int]
    internal let completeMessage: String
    internal let univercityName: String
    internal let badgePath: String
    
    internal var progress: Int {
        get {
            var comfirmedSpots: [Int] = []
            requiredCheckin.forEach { id in
                Checkin.findWith(id: id).success { checkins in
                    if checkins.count > 0 { comfirmedSpots.append(id) }
                }
            }
            let num = Float(comfirmedSpots.count) / Float(requiredCheckin.count)
            return Int(num * 100)
        }
    }
    
    internal init(json: JSON) {
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
        self.difficulity = json["difficulity"].intValue
        self.completeMessage = json["completeMessage"].stringValue
        self.univercityName = json["univercityName"].stringValue
        self.badgePath = json["badgePath"].stringValue
        var requiredCheckin: [Int] = []
        if let checkinArray = json["requiredCheckin"].arrayObject {
            for checkin in checkinArray {
                if let checkin = checkin as? Int {
                    requiredCheckin.append(checkin)
                } else {
                    requiredCheckin = []
                }
            }
        } else {
            requiredCheckin = []
        }
        self.requiredCheckin = requiredCheckin
    }
    
    internal init(id: Int, title: String, description: String, difficulity: Int, requiredCheckin: [Int], completeMessage: String, univercity: String, badgePath: String) {
        self.id = id
        self.title = title
        self.description = description
        self.difficulity = difficulity
        self.requiredCheckin = requiredCheckin
        self.completeMessage = completeMessage
        self.univercityName = univercity
        self.badgePath = badgePath
    }
    
}
