import UIKit

class ParentTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.tabBar.barTintColor = UIColor.white
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is DummyViewController {
            if let modalVC = tabBarController.storyboard?.instantiateViewController(withIdentifier: "CheckinViewController") {
                tabBarController.present(modalVC, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

class DummyViewController: UIViewController {}
