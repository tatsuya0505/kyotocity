import UIKit
import Bond
import SwiftyJSON
import Alamofire
import RxSwift
import RxCocoa

final class MissionViewModel {
    
    static let sharedInstance = MissionViewModel()
    
    internal enum Sort: Int {
        case difficulity = 1
        case complete
    }
    
    private init() {}
    
    internal let missions = Variable<[Mission]>([])
    
    internal func reload() {
        self.missions.value.removeAll()
        guard let path = Bundle.main.path(forResource: "mission", ofType: "json") else { return }
        let fileHandle = FileHandle(forReadingAtPath: path)
        let data = fileHandle!.readDataToEndOfFile()
        let jsons = JSON(data: data)
        for index in 0...jsons.count-1 {
            let mission = Mission(json: jsons[index])
            self.missions.value.append(mission)
            print("hogehoge")
            print(missions)
        }
    }
    
    internal func find(with: Int) -> Mission? {
        var foundMission: Mission?
        self.missions.value.forEach { mission in
            if let _ =  mission.requiredCheckin.index(of: with) {
                foundMission = mission
            }
        }
        return foundMission
    }
    
    internal func sort(type: Sort) {
        switch type {
        case .difficulity:
            let sortedMissions = self.missions.value.sorted { $0.difficulity > $1.difficulity }
            self.missions.value.removeAll()
            sortedMissions.forEach { self.missions.value.append($0) }
        case .complete:
            let sortedMissions = self.missions.value.sorted { $0.progress > $1.progress }
            self.missions.value.removeAll()
            sortedMissions.forEach { self.missions.value.append($0) }
        }
    }


}
