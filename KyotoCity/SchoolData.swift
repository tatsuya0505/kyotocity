//
//  SchoolData.swift
//  KyotoCity
//
//  Created by takahashi tomoki on 2016/11/13.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit



class SchoolData {
    
    var name : String!
    var point : Int!
    var address : String!
    var eventPoint : Int!
    var image : String!
    
    init(name : String , point : Int , address : String , eventPoint : Int , image : String) {
        self.name = name
        self.point = point
        self.address = address
        self.eventPoint = eventPoint
        self.image = image
    }

}
