import UIKit

class CollectionViewController: UIViewController {
    
    override func loadView() {
        view = UIView.instantiateFromNib("CollectionView", owner: self)
        view.frame = UIScreen.main.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
