import UIKit
import Bond
import SegueContext

class MissionDetailViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var univercitiesName: UILabel!
    
    
    override func loadView() {
        view = UIView.instantiateFromNib("MissionDetailView", owner: self)
        view.frame = UIScreen.main.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        guard let mission: Mission = self.contextValue() else { return }
//        tableView.delegate = self
//        tableView.tableHeaderView = UIView.instantiateFromNib("MissionDetailTableHeader", owner: self)
//        if let header = tableView.tableHeaderView as? MissionDetailTableHeader {
//            header.mission = mission
//        }
        createContents()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.performSegueWithIdentifier("missiondetail", context: missions[indexPath.section][indexPath.row])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createContents(){
        let appDel = UIApplication.shared.delegate as! AppDelegate
        titleLabel.text = appDel.detailTitle
        descriptionLabel.text = appDel.detailDescription
        univercitiesName.text = appDel.detailUnivercities
    }
    
}
