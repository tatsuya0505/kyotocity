import RealmSwift
import SwiftTask

class Checkin: Object {
    
    typealias CheckinTask = Task<Void, Checkin, NSError>
    typealias CheckinsTask = Task<Void, [Checkin], NSError>
    
    // MARK: - Properties
    dynamic var id = 0
    dynamic var photoPath = ""
    dynamic var date = NSDate()
    dynamic var locationX: Double = 0
    dynamic var locationY: Double = 0
    dynamic var weather = 0
    
    override class func primaryKey() -> String {
        return "photoPath"
    }
    
    // MARK: - Publics
    convenience init(id: Int, photoPath: String, date: NSDate, locationX: Double, locationY: Double, weather: Int) {
        self.init()
        self.id = id
        self.photoPath = photoPath
        self.date = date
        self.locationX = locationX
        self.locationY = locationY
        self.weather = weather
    }
    
    static func findAll() -> CheckinsTask {
        return CheckinsTask { progress, fulfill, reject, configure in
            let realm = RealmFactory.sharedInstance.realm()
            let checkins = realm.objects(Checkin.self).sorted(byProperty: "id", ascending: false)
            fulfill(checkins.map { $0 })
        }
    }
    
    static func findWith(id: Int) -> CheckinsTask {
        return CheckinsTask { progress, fulfill, reject, configure in
            let realm = RealmFactory.sharedInstance.realm()
            let checkins = realm.objects(Checkin.self).filter("id == \(id)")
            fulfill(checkins.map { $0 })
        }
    }
    
    static func deleteAll() {
        let realm = RealmFactory.sharedInstance.realm()
        do {
            try realm.write {
                realm.delete(realm.objects(Checkin.self))
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func save() -> CheckinTask {
        return CheckinTask { progress, fulfill, reject, configure in
            let realm = RealmFactory.sharedInstance.realm()
            do {
                try realm.write {
                    realm.add(self)
                }
                fulfill(self)
            } catch let error as NSError {
                reject(error)
            }
        }
    }
    
}
