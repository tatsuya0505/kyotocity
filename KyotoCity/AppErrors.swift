import Foundation

struct AppErrors {
    
    static let LocationError = NSError(
        domain: "locationError",
        code: 0,
        userInfo: nil
    )
    
}
