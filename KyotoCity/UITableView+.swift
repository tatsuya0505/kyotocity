import UIKit

extension UITableView {
    
    func registerCell<T: UITableViewCell>(type: T.Type) {
        let nib = UINib(nibName: type.className, bundle: nil)
        register(nib, forCellReuseIdentifier: type.className)
    }
    
    func dequeueCell<T: UITableViewCell>(type: T.Type, indexPath: NSIndexPath) -> T? {
        return dequeueReusableCell(withIdentifier: type.className, for: indexPath as IndexPath) as? T
    }
    
    func getCell<T: UITableViewCell>(type: T.Type, indexPath: NSIndexPath) -> T? {
        return cellForRow(at: indexPath as IndexPath) as? T
    }
    
}
