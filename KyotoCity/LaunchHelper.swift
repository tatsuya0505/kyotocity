import UIKit

protocol LaunchHelper: class {
    
    // MARK: - Publics
    func setup(application: UIApplication)
    
}
