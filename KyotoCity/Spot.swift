import SwiftyJSON
import CoreLocation

struct Spot {
    
    internal let id: Int
    internal let title: String
    internal let locationX: Double
    internal let locationY: Double
    internal let description: [String: String]
    
    internal var location: CLLocation {
        get {
            return CLLocation(latitude: self.locationX, longitude: self.locationY)
        }
    }
    
    internal init(json: JSON) {
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.locationX = json["locationX"].doubleValue
        self.locationY = json["locationY"].doubleValue
        let dic = json["description"].dictionaryObject
        if let dic = dic as? [String: String] {
            self.description = dic
        } else {
            self.description = [:]
        }
    }
    
    internal init(id: Int, title: String, locationX: Double, locationY: Double, description: [String: String]) {
        self.id = id
        self.title = title
        self.locationX = locationX
        self.locationY = locationY
        self.description = description
    }
    
}
