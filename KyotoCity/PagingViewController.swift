//
//  PagingViewController.swift
//  KyotoCity
//
//  Created by takahashi tomoki on 2016/11/13.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit
import PagingMenuController

class PagingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let options = SearchPagingMenuOptions()
        let pagingMenuController = self.childViewControllers.first as! PagingMenuController
        pagingMenuController.setup(options)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    



}
