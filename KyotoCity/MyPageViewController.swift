//
//  MyPageViewController.swift
//  KyotoCity
//
//  Created by takahashi tomoki on 2016/11/13.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyPageViewController: UIViewController {
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var pointSum: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        setting()

        DummyViewModel.sharedInstance.pointSum.asObservable()
            .bindNext({ [weak self] test in
                self?.pointSum.text = "\(test)"
                
            })
            .addDisposableTo(disposeBag)
    
        
       
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setting(){
        iconImageView.layer.cornerRadius = iconImageView.frame.height / 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    static func instantiate() -> MyPageViewController {
        let storyBoard = UIStoryboard(name: "MyPage", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: "myPage") as! MyPageViewController
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
