import UIKit


extension UIColor {
    
    class func carnation() -> UIColor {
        return UIColor.rgbString(hexString: "fd7586", alpha: 1.0)
    }
    
    class func warmPink() -> UIColor {
        return UIColor.rgbString(hexString: "f75b6e", alpha: 1.0)
    }
    
    class func rgbColor(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red:   CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >>  8) / 255.0,
            blue:  CGFloat( rgbValue & 0x0000FF)        / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    class func rgbString(hexString str: String, alpha: CGFloat) -> UIColor {
        let range = NSMakeRange(0, (str as NSString).length)
        let hex = (str as NSString).replacingOccurrences(of: "[^0-9a-fA-F]", with: "", options: NSString.CompareOptions.regularExpression, range: range)
        var color: UInt32 = 0
        Scanner(string: hex).scanHexInt32(&color)
        return rgbColor(UInt(color))
    }
    
    
    
}
