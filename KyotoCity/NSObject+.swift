//
//  NSObject+.swift
//  KyotoCity
//
//  Created by 西川達哉 on 2016/11/07.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import Foundation

extension NSObject {
    
    class var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
    
    var className: String {
        return type(of: self).className
    }
    
}

