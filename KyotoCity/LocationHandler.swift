import SwiftLocation
import SwiftTask
import CoreLocation

typealias DistanceKm = Float
typealias DistanceTask = Task<Void, DistanceKm, NSError>
typealias SpotsTask = Task<Void, [Spot], NSError>


final class LocationHandler {
    
    static let sharedInstance = LocationHandler()
    
    private init() {}
    
    func distance(form from: Spot) -> DistanceTask {
        return DistanceTask { progress, fulfill, reject, configure in
            Location.getLocation(withAccuracy: .block, onSuccess: { foundLocation in
                let distance = foundLocation.distance(from: from.location)
                fulfill(Float(distance/1000))
            }) { (lastValidLocation, error) in
                reject(AppErrors.LocationError)
            }
        }
    }
    
    func spots(within km: Float) -> SpotsTask {
        return SpotsTask { progress, fulfill, reject, configure in
            Location.getLocation(withAccuracy: .block, onSuccess: { foundLocation in
                let spots = SpotViewModel.sharedInstance.spots.value.filter {
                    Float($0.location.distance(from: foundLocation))/1000 < km
                }
                fulfill(spots)
            }) { (lastValidLocation, error) in
                reject(AppErrors.LocationError)
            }
        }
    }
    
}
