//
//  PagingOption.swift
//  Kejiban
//
//  Created by takahashi tomoki on 2016/10/09.
//  Copyright © 2016年 tatsuya. All rights reserved.
//

import Foundation
import PagingMenuController




private var pagingControllers: [UIViewController] {
    
    let list = ListViewController.instantiate()
    let myPage = MyPageViewController.instantiate()
    
    
    return [myPage , list]
}

struct MenuItemQuestion: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
        let title = MenuItemText(text: "My Point", color: .black, selectedColor: .black, font: UIFont.systemFont(ofSize: 14), selectedFont: UIFont.boldSystemFont(ofSize: 14))
        return .text(title: title)
    }
}
struct MenuItemTag: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
        let title = MenuItemText(text: "履歴", color: .black, selectedColor: .black, font: UIFont.systemFont(ofSize: 14), selectedFont: UIFont.boldSystemFont(ofSize: 14))
        return .text(title: title)
    }
}



struct SearchPagingMenuOptions: PagingMenuControllerCustomizable {
    var componentType: ComponentType {
        return .all(menuOptions: MenuOptions(), pagingControllers: pagingControllers)
    }
    
    struct MenuOptions: MenuViewCustomizable {
        var displayMode: MenuDisplayMode {
            return .segmentedControl
        }
        
        var focusMode: MenuFocusMode {
            return .underline(height: 3, color: .gray, horizontalPadding: 0, verticalPadding: 0)
        }
        
        var animationDuration: TimeInterval {
            return 0.1
        }
        
        var height: CGFloat {
            return 40
        }
        
        var backgroundColor: UIColor {
            return .white
        }
        var itemsOptions: [MenuItemViewCustomizable] {
            return [MenuItemQuestion(), MenuItemTag()]
        }
        
        
    }
    
}

