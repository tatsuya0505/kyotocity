import UIKit
import SwiftyJSON
import Bond
import Alamofire
import RxSwift
import RxCocoa

final class SpotViewModel {
    
    static let sharedInstance = SpotViewModel()
    
    internal let spots = Variable<[Spot]>([])
    
    private init() {}
    
    internal func reload() {
        guard let path = Bundle.main.path(forResource: "spot", ofType: "json") else { return }
        let fileHandle = FileHandle(forReadingAtPath: path)
        let data = fileHandle!.readDataToEndOfFile()
        let jsons = JSON(data: data)
        for index in 0...jsons.count-1 {
            let spot = Spot(json: jsons[index])
            self.spots.value.append(spot)
        }
    }
    
    internal func spot(with id: Int) -> Spot? {
        return self.spots.value.filter { $0.id == id }.first
    }


}
