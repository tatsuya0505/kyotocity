import UIKit
import Bond
import RxSwift
import RxCocoa

class CheckinViewController: UIViewController  {
    
    private enum CheckinStatus {
        case gpswaiting
        case gpsfailure
        case checkinFailure
        case checkinSuccess
    }
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var buttonText: UILabel!
    @IBOutlet weak var cancellButton: UIButton!
    
    
    private var checkinSpot: Spot?
    private let bag = DisposeBag()
//    private var pairCheckinTask: PairTask? = nil
    
    private let status = Variable<CheckinStatus>(.checkinSuccess)
    
    override func loadView() {
        view = UIView.instantiateFromNib("CheckinView", owner: self)
        view.frame = UIScreen.main.bounds
    }
    
    let dummy = DummyViewModel.sharedInstance
    
    func didTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
       
    override func viewDidLoad() {
        super.viewDidLoad()

        
        checkin()
        status.asObservable().bindNext({ [weak self] status in
            switch status {
            case .gpswaiting:
                self?.iconImageView.image = UIImage(named: "checkin_gps_loading")
                self?.textLabel.text = "現在地を調べています"
                self?.buttonText.text = ""
                self?.iconButton.setImage(UIImage(named: "checkin_close_bottom_gray"), for: UIControlState.normal)
                self?.iconButton.bnd_tap
                    .observeNext {_ in
                        self?.dismiss(animated: true, completion: nil)
                    }
                
            case .gpsfailure:
                self?.iconImageView.image = UIImage(named: "checkin_gps_loading")
                self?.textLabel.text = "GPS読み込みに失敗しました"
                self?.buttonText.text = "もう一度試す"
                self?.iconButton.setImage(UIImage(named: "checkin_retry"), for: .normal)
                self?.iconButton.bnd_tap
                    .observeNext {_ in
                        self?.checkin()
                    }
                
            case .checkinFailure:
                self?.iconImageView.image = UIImage(named: "checkin_gps_loading")
                self?.textLabel.text = "ここはチェックインスポットではありません"
                self?.buttonText.text = "スポットを探す"
                self?.iconButton.setImage(UIImage(named: "checkin_to_mission"), for: .normal)
                self?.iconButton.bnd_tap
                    .observeNext {_ in
                        self?.dismiss(animated: true, completion: nil)
                    }
                
            case .checkinSuccess:
//                guard let spot = self?.checkinSpot else { return }
                self?.iconImageView.image = UIImage(named: "checkin_gps_loading")
//                self?.textLabel.text = "チェックインスポット \n「\(spot.title)」です"
                self?.textLabel.text = "チェックインスポットです"
                self?.textLabel.textColor = Colors.MainColor
                self?.buttonText.text = ""
                self?.buttonText.textColor = Colors.MainColor
//                self?.view.backgroundColor = Colors.MainColor
                self?.iconButton.setImage(UIImage(named: "checkin_photo_accept"), for: .normal)
                self?.iconButton.bnd_tap
                    .observeNext {_ in
//                        let checkinComplete = CheckinCompleteViewController()
//                        self?.present(checkinComplete, animated: false, completion: nil)
                         self?.iconImageView.image = UIImage(named: "checkin_complete-1")

                        let model = SchoolData(name: "京町屋大学", point: 100, address: "京都市上京区福大明神町128", eventPoint: 30, image: "machiya")
                        self?.textLabel.text = model.name


                        DummyViewModel.sharedInstance.pointSum.value += model.point
                        
                        DummyViewModel.sharedInstance.model.value.append(model)
                        


                    }
                self?.cancellButton.bnd_tap
                    .observeNext {_ in
                        self?.dismiss(animated: true, completion: nil)
                    }

            }
            self?.view.layoutSubviews()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func checkin() {
        LocationHandler.sharedInstance.spots(within: 1).success { spots in
            if spots.isEmpty {
                self.status.value = .checkinFailure
            } else {
                // TODO:
                self.checkinSpot = spots[0]
//                self.status(.pairWaiting)
//                self.pairing()
            }
        }
    }
    
//    private func pairing() {
//        let d = PairCheckedInData(locationX: 10, locationY: 11, spotID: 1)
//        pairCheckinTask = MCPairConnector.sharedInstance.checkInWithPair(at: d).success {
//            dispatch_async(dispatch_get_main_queue(), {
//                self.status.next(.checkinSuccess)
//            })
//            }.failure { _ in
//                print("Failure")
//        }
//        
//        MCPairConnector.sharedInstance.sendImage(UIImage(named: "sample2")!).success({
//            print ("image ok")
//        })
//        
//        MCPairConnector.sharedInstance.eventReciever = self
//        MCPairConnector.sharedInstance.dataReciever = self
//        
//    }
//    
//    func reciveEvent(event: PairEvent) {
//        switch event {
//        case let .pairCheckIn(at):
//            print(at)
//            dispatch_async(dispatch_get_main_queue(), {
//                self.checkinSpot = SpotViewModel.sharedInstance.spots[at.spotID]
//                self.pairCheckinTask?.cancel()
//                self.pairCheckinTask = nil
//                self.status.next(.checkinSuccess)
//            })
//        }
//    }
    
    func reciveImage(image: UIImage) {
        print ("get image")
    }
}
